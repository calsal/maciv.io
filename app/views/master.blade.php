<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
		<title>@yield('title')</title>
		<!-- LOAD JS THAN NEED EARLY LOAD -->
		{{ HTML::script('js/jquery-1.js') }}
		{{ HTML::script('js/bootstrap.min.js') }}
		{{ HTML::script('js/timeline.js') }}
		<!-- LOAD CSS -->
		{{ HTML::style('css/bootstrap.min.css')}}
		{{ HTML::style('css/timeline.css')}}
		{{ HTML::style('css/default.css')}}
	</head>
	<body>
		<section>
			<div class="bcg">
				<h1>MACIV.IO</h1>
				<div class="centered text_content">
					<h2>Timeline of memories</h2>
					<h3>Beta</h3>
					<!-- CONTENT GOES HERE -->
	    			@yield('content') 
	    		</div>
			</div>
		</section>
		<!-- LOAD JS -->
		{{ HTML::script('js/jquery.js') }}
		{{ HTML::script('js/default.js') }}
		 <!-- SCRIPTS GOES HERE -->
	    @yield('scripts')
	</body>
</html>
