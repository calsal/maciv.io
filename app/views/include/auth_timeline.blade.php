<h2 id="timeline-start">Your timeline</h2>

<div class="timeline" data-tl-autoinit>
	@foreach ($content as $entry)
		<div data-tl-orient="vertical" data-tl-image="url(img/{{$entry->source}}-logo.png)" class="timeline-block tl-orient-vertical">
			<div class="timeline-content bounce-in">
				<h2>{{ $entry->created_time }}</h2>
				@if (!empty($entry->location_name)) 
					@if (strcmp($entry->source, "instagram") == 0)
						<h3><a target="_blank" href="http://maps.google.com/?q={{ $entry->latitude }}, {{ $entry->longitude }}">{{ $entry->location_name }}</a></h3>
					@else
						<h3><a target="_blank" href="http://maps.google.com/?q={{ $entry->location_name }}">{{ $entry->location_name }}</a></h3>
					@endif
				@endif
				@if (!empty($entry->media_url_https))
					<img src="{{ $entry->media_url_https }}">
				@endif
				@if (!empty($entry->caption_text))
					<h3>{{ $entry->caption_text }}</h3>
				@endif
				@if (strcmp($entry->source, 'instagram') == 0)
					<p>{{ $entry->likes_count }} likes and {{ $entry->comments_count }} comments.</p>
				@else
					<p>{{ $entry->retweet_count}} retweets and {{ $entry->favorite_count }} favorites.</p>
				@endif
				
			</div>
		</div>
	@endforeach
</div>
<hr>
<p>That was it!</p> <a href="javascript:void(0)" id="top-link">Want to go to the top again?</a>