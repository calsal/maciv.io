<div class="alert alert-danger alert-dismissible center-block" role="alert">
	<button type="button" class="close" data-dismiss="alert">×</button>
	<p class="text-center">Oh no! We have a problem with your filter settings: <br>
		{{ Session::get('filter_error') }}</strong></p>
		<p>No changes has been made.</p>
</div>