<div class="login-wrapper">	
	@if (Session::has('success'))
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			{{ Session::get('success') }}.
		</div>
	@endif

	@if ($errors->has())
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert">×</button>
			<strong>Something went wrong!</strong>
			@foreach ($errors->all() as $error)
				<p>{{ $error }}</p>
			@endforeach
			<strong>Try again!</strong>
		</div>
	@endif

	<h3>Relive your favorite memories using data from social media.</h3>
	<h3>We´re still in development and the signup is closed.</h3>
	<h3>Come back soon though, we will open really soon!</h3>
	<form class="form-inline" role="form" action="users/store" method="POST">
		<div class="form-group">
			<div class="input-group">
				<label class="sr-only" for="email">Email address</label>
				<input type="email" class="form-control" name="email" id="email" placeholder="Email" disabled>
			</div>
		</div>
		<div class="form-group">
			<div class="input-group">
				<label class="sr-only" for="password">Password</label>
				<input type="password" class="form-control" name="password" id="password" placeholder="Password" disabled>
			</div>
		</div>
		<button type="submit" class="btn btn-success disabled">Create account</button>
	</form>
	<a class="btn btn-success disabled" href="/users/login" role="button">Already have an account?</a>
</div>