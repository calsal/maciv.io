<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	/**
	 * Return if any service has been added.
	 *
	 * @return boolean
	 */
	public function isServiceAdded()
	{
		return $this->has_added_service;
	}

	/**
	 * Return the media for this users timeline.
	 *
	 * @return array
	 */
	public function getMedia()
	{
		return Media::where('owner', '=', $this->id)->get()->toArray();
	}

	/**
	 * Return the tweets for this users timeline.
	 *
	 * @return array
	 */
	public function getTweets()
	{
		return Tweet::where('owner', '=', $this->id)->get()->toArray();
		// return Tweet::all()->toArray();
	}

	/**
	 * Return if the user is using Instagram for the timeline
	 *
	 * @return boolean
	 */
	public function isInstagramAdded()
	{
		return $this->instagram_added;
	}


	/**
	 * Return if the user is using Instagram for the timeline
	 *
	 * @return boolean
	 */
	public function isInstagramUsedForTimeline()
	{
		return $this->instagram_used_for_timeline;
	}

	/**
	 * Return the decrypted Instagram access token for the user.
	 *
	 * @return string
	 */
	public function getInstagramAccessToken()
	{
		return Crypt::decrypt($this->instagram_access_token);
	}

	/**
	 * Return the users id-number on Instagram.
	 *
	 * @return string
	 */
	public function getInstagramId()
	{
		return $this->instagram_id;
	}

	/**
	 * Return if the user is using Instagram for the timeline
	 *
	 * @return boolean
	 */
	public function isTwitterAdded()
	{
		return $this->twitter_added;
	}

	/**
	 * Return if the user is using Instagram for the timeline
	 *
	 * @return boolean
	 */
	public function isTwitterUsedForTimeline()
	{
		return $this->twitter_used_for_timeline;
	}

	/**
	 * Return the decrypted Twitter access token for the user
	 *
	 * @return string
	 */
	public function getTwitterAccessToken() 
	{
		return Crypt::decrypt($this->twitter_access_token);
	}

	/**
	 * Return the decrypted Twitter access token secret for the user
	 *
	 * @return string
	 */
	public function getTwitterAccessTokenSecret() 
	{
		return Crypt::decrypt($this->twitter_access_token_secret);
	}

	/**
	 * Return the users id-number on Twitter.
	 *
	 * @return string
	 */
	public function getTwitterId()
	{
		return $this->twitter_id;
	}

	/**
	 * Return whether or not all services are activated for the timeline
	 *
	 * @return boolean
	 */
	public function isEverythingUsedForTimeline()
	{
		return $this->twitter_used_for_timeline && $this->instagram_used_for_timeline;
	}


}
