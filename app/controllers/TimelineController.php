<?php

class TimelineController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| TimelineController
	|--------------------------------------------------------------------------
	|
	| This controller is used to handle actions regarding the users timeline.
	| Current actions: 
	|--------------------------------------------------------------------------
	| 	create - GET - Creates the timeline from scratch.
	|	update - POST - Update the filter settings for the timeline.
	|	
	|
	*/

/*
|--------------------------------------------------------------------------
| create - GET - Creates the timeline from scratch.
|--------------------------------------------------------------------------
|
*/
	public function getCreate()
	{
		$user = Auth::user();
		$user->has_timeline = true;
		// Twitter
		if (Auth::user()->isTwitterAdded()) {
    		$this->createTweetEntries();
    		$user->twitter_used_for_timeline = true;
    	}
		// Instagram
		if (Auth::user()->isInstagramAdded()) {
			$this->createMediaEntries();
			$user->instagram_used_for_timeline = true;
		}
		$user->save();

		return Redirect::to('/');
	}

/*
|----------------------------------------------------------------------------
| Helper function that creates media entrys in DB based on Instagram response
|----------------------------------------------------------------------------
|
*/
	private function createMediaEntries()
	{
		// Get media from Instagram API
		Instagram::setAccessToken(Auth::user()->getInstagramAccessToken());
		$content = Instagram::getUserMedia(Auth::user()->getInstagramId(), 30);
		// Save in DB
		foreach ($content->data as $data) {
			$media = new Media;
			$media->owner = Auth::user()->id;
			$media->created_time = date('Y-m-d', $data->created_time);
			if (isset($data->location)) {
				if (isset($data->location->name)) {
					$media->location_name = $data->location->name;
				}
				$media->latitude = $data->location->latitude;
				$media->longitude = $data->location->longitude;
			}
			$media->comments_count = $data->comments->count;
			$media->likes_count = $data->likes->count;
			$media->media_low_resolution = $data->images->low_resolution->url;
			$media->media_thumbnail = $data->images->thumbnail->url;
			$media->media_url_https = $data->images->standard_resolution->url;
			if (isset($data->caption)) {
				$media->caption_text = $data->caption->text;
			}
			$media->image_id = $data->id;
			$media->link = $data->link;
			$media->source = 'instagram';
			$media->save();
			Auth::user()->instagram_media_received = true;
			Auth::user()->save();
		}
	}
/*
|----------------------------------------------------------------------------
| Helper function that creates tweet entrys in DB based on Twitter response
|----------------------------------------------------------------------------
|
*/
	private function createTweetEntries()
	{
		// Get Tweets from Twitter API
		Twitter::setOAuthToken(Auth::user()->getTwitterAccessToken());
    	Twitter::setOAuthTokenSecret(Auth::user()->getTwitterAccessTokenSecret());
    	$content = Twitter::statusesUserTimeline(Auth::user()->getTwitterId(), null, null, 30);
		$inputObject = json_decode(json_encode($content));
		// Save in DB
		foreach ($inputObject as $data) {
			if (Auth::user()->isInstagramAdded()) {
				if (strcmp($data->source, '<a href="http://instagram.com" rel="nofollow">Instagram</a>') == 0) {
					// If the user as added Instagram as a service
					// and the Tweet source is Instagram
					// we will use the entry from Instagram instead
					continue;
				}
			}
			$tweet = new Tweet;
			$tweet->owner = Auth::user()->id;
			$tweet->created_time = date('Y-m-d', strtotime($data->created_at) );
			if (isset($data->place)) {
				$tweet->location_name = $data->place->full_name;
			}
			$tweet->retweet_count = $data->retweet_count;
			$tweet->favorite_count = $data->favorite_count;
			if (isset($data->entities->media)) {
				$tweet->media_url_https = $data->entities->media[0]->media_url_https;
			}
			$tweet->profile_image_url_https = $data->user->profile_image_url_https;
			$tweet->caption_text = $data->text;
			$tweet->source = 'twitter';
			$tweet->save();
			Auth::user()->twitter_tweets_received = true;
			Auth::user()->save();
		}
	}

/*
|--------------------------------------------------------------------------
| update - POST - Update the filter settings for the timeline.
|--------------------------------------------------------------------------
|
*/
	public function postUpdate() 
	{
		if (!Input::has('instagram') && !Input::has('twitter')) {
			return Redirect::to('/')->with('filter_error', "You need at least one service for your timeline!");
		}
		$user = Auth::user();
		$user->instagram_used_for_timeline = Input::has('instagram');
		if (Input::has('instagram')) {
			// Update whether or not to use Instagram for the timeline
			if (!$user->instagram_media_received) {
				// If we dont already have have media from instagram we need to get it
				$this->createMediaEntries();
			}
		}
		$user->twitter_used_for_timeline = Input::has('twitter');
		if (Input::has('twitter')) {
			// Update whether or not to use Twitter for the timeline
			if (!$user->twitter_tweets_received) {
				$this->createTweetEntries();
			}
		}
		$user->save();
		return Redirect::to('/');
	}
}