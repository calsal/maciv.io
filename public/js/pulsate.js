$.fn.pulsate = function(opt) {
    var option = {
        dimension: 180,
        color: "lightblue",
        width: 1,
        align: "center",
        speed: 1,
        'function': "ease",
        fill: "rgba(0, 0, 0, 0)",
        iterations: "infinite",
        overflow: "hidden",
        zindex: -1,
        opacity: 1
    };
    $.extend(true, option, opt);
    var pulse = $("<div class='pulse'></div>");
    pulse.css('-webkit-animation-duration', option.speed + 's').css("-webkit-animation-timing-function", option['function']).css('-webkit-animation-iteration-count', option.iterations);
    pulse.css('border-color', option.color).css("border-width", option.width + "px").css("background-color", option.fill);
    pulse.css('z-index', option.zindex);
    
    var resize = function(dim) {
        pulse.css('height', dim + "px").css('width', dim + "px");
        if (option.align === "center") {
            pulse.css('margin-left', "-" + dim / 2 + "px");
        } else if (option.align === "left") {
            pulse.css('left', '-' + dim / 2 + "px");
        }
        pulse.css('margin-top', "-" + dim / 2 + "px");
    };
    resize(option.dimension);
    

    if ($(this).css('position') === '') {
        $(this).css('position', 'relative');
    }
    $(this).css('overflow', option.overflow);
    $(this).append($("<div></div>").css('opacity', option.opacity).append(pulse));
    $(this).bind('resize', function(dim) {
        resize(dim);
    });
    return pulse;
};